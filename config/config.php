<?php

const DEFAULT_NAMESPACE = 'DanielAnjos\WCrypto\Http\Controllers';

const LOG_DIR = __DIR__ . '/../Logs/app.log';

const KEY_HASH_01 = 'a793512b591692978b60fc5fc6fc396b';

const KEY_HASH_02 = '1cc6a88e0bd964974cdee209657fd2c4';

const JWT_KEY = '97b77dba84fd26c9d3298102627584cd';

const DB_CONNECTION= 'mysql';
const DB_HOST= 'localhost';
const DB_DBNAME= 'danielanjos_wcrypto';
const DB_USERNAME= 'root';
const DB_PASSWORD= 'Novasenha1@';

const DB_CONNECTION_TEST = 'mysql';
const DB_HOST_TEST = 'localhost';
const DB_DBNAME_TEST = 'danielanjos_wcrypto_test';
const DB_USERNAME_TEST = 'root';
const DB_PASSWORD_TEST = 'Novasenha1@';

const TEST_VALID_TOKEN = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiMSIsIm5hbWUiOiJZdDdzeHNFajlUZ1wvckFlWEpwT0dZUT09IiwiY3BmX2NucGoiOiJCOFBGME1jaEU1cFNXXC9TTVRSMGJnbmY4RXkrT1BDdUpvd3RBd3Nkc0d4VT0iLCJhY2NvdW50X251bWJlciI6IkVQK0Iwbkd1eE5JdnB0bmtQR1dvQnc9PSJ9.3K1xMMmf_8yC_lOV-0guNvDC5IZtsF1oFSilf0diU0w';
const TEST_INVALID_TOKEN = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiMyIsIm5hbWUiOiJZdDdzeHNFajlUZ1wvckFlWEpwT0dZUT09IiwiY3BmX2NucGoiOiJMK2Z4ZHpEMW02TlZLZjE5ZEJJZ2pMV0V3M0pkK1U2NjVUaktyeHhFXC9ZRT0iLCJhY2NvdW50X251bWJlciI6IlN6bXZLZTFyTmp3dXRjbE9pS3FwMEE9PSJ9.jmPxsm5iFfpT3PKoQHbOow2_uC7MaHh_rQ4cqu-J4Tg';
