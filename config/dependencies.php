<?php

use DanielAnjos\WCrypto\Http\Contracts\AccountsRepositoryInterface;
use DanielAnjos\WCrypto\Http\Contracts\SessionsRepositoryInterface;
use DanielAnjos\WCrypto\Http\Contracts\TransactionsRepositoryInterface;
use DanielAnjos\WCrypto\Http\Contracts\UsersRepositoryInterface;
use DanielAnjos\WCrypto\Http\DAOs\AccountDAO;
use DanielAnjos\WCrypto\Http\DAOs\SessionsDAO;
use DanielAnjos\WCrypto\Http\DAOs\TransactionDAO;
use DanielAnjos\WCrypto\Http\DAOs\UserDAO;
use DanielAnjos\WCrypto\Http\Repositories\AccountsRepository;
use DanielAnjos\WCrypto\Http\Repositories\SessionsRepository;
use DanielAnjos\WCrypto\Http\Repositories\TransactionsRepository;
use DanielAnjos\WCrypto\Http\Repositories\UsersRepository;
use DI\ContainerBuilder;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

$cacheDir = sys_get_temp_dir();

$containerBuilder = new ContainerBuilder();

$containerBuilder->addDefinitions([
    AccountsRepositoryInterface::class => function () {
        $accountDAO = new AccountDAO();
        return new AccountsRepository($accountDAO);
    },
    UsersRepositoryInterface::class => function () {
        $userDAO = new UserDAO();
        return new UsersRepository($userDAO);
    },
    TransactionsRepositoryInterface::class => function () {
        $transactionDAO = new TransactionDAO();
        $accountDAO = new AccountDAO();
        return new TransactionsRepository($transactionDAO, $accountDAO);
    },
    SessionsRepositoryInterface::class => function () {
        $sessionsDAO = new SessionsDAO();
        return new SessionsRepository($sessionsDAO);
    },
    Psr\Log\LoggerInterface::class => DI\factory(function () {
        $logger = new Logger('wcrypto');

        $fileHandler = new StreamHandler(LOG_DIR, Logger::DEBUG);
        $fileHandler->setFormatter(new LineFormatter());
        $logger->pushHandler($fileHandler);

        return $logger;
    }),
]);

$container = $containerBuilder->enableCompilation($cacheDir)
    ->writeProxiesToFile(true, $cacheDir . '/proxies')
    ->useAutowiring(true)
    ->build();
