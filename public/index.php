<?php

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../config/cors.php';
require __DIR__ . '/../config/dependencies.php';

\DanielAnjos\WCrypto\Router::enableDependencyInjection($container);

\DanielAnjos\WCrypto\Router::start();
