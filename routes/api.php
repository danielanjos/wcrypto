<?php

use Pecee\SimpleRouter\SimpleRouter as Router;

Router::group([
    'exceptionHandler' =>  \DanielAnjos\WCrypto\Exceptions\ExceptionHandler::class
], function () {

    Router::group(
        [
            'prefix' => '/api',
            'middleware' => \DanielAnjos\WCrypto\Http\Middlewares\Authenticate::class
        ],
        function () {
            Router::post('/transactions', 'TransactionsController@create');
        }
    );

    Router::post('/api/users/new', 'UsersController@create');
    Router::post('/api/sessions/new', 'SessionsController@authenticate');
});
