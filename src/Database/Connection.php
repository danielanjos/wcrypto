<?php

namespace DanielAnjos\WCrypto\Database;

use DanielAnjos\WCrypto\Exceptions\AuthorizationException;
use DanielAnjos\WCrypto\Exceptions\ConnectionException;

class Connection
{
    public static function connect()
    {
        try {
            $connection = new \PDO(
                DB_CONNECTION .
                    ":host=" . DB_HOST .
                    ";dbname=" . DB_DBNAME,
                DB_USERNAME,
                DB_PASSWORD,
                array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")
            );


            return $connection;
        } catch (\Exception $e) {
            throw new ConnectionException();
        }
    }

    public static function closeConnection(&$conexao)
    {
        $conexao = null;
    }
}
