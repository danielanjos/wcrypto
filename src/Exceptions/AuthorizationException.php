<?php

namespace DanielAnjos\WCrypto\Exceptions;

class AuthorizationException extends \Exception
{
    protected $message = 'Unauthorized';
}
