<?php

namespace DanielAnjos\WCrypto\Exceptions;

class ConnectionException extends \Exception
{
    protected $message = 'Failed to connect to the database';
}
