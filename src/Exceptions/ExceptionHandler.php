<?php

namespace DanielAnjos\WCrypto\Exceptions;

use Exception;
use InvalidArgumentException;
use Pecee\Http\Request;
use Pecee\SimpleRouter\Exceptions\NotFoundHttpException;
use Pecee\SimpleRouter\Handlers\IExceptionHandler;

class ExceptionHandler implements IExceptionHandler
{
    public function handleError(Request $request, Exception $error): void
    {

        if($error instanceof InvalidArgumentException){
            response()->httpCode(400)->json(['error' => $error->getMessage()]);
            return;
        }
        if ($error instanceof AuthorizationException) {
            response()->httpCode(401)->json(['error' => $error->getMessage()]);
            return;
        } elseif ($error instanceof NotFoundHttpException) {
            response()->httpCode(404)->json(['error' => 'Route not found']);
            return;
        }elseif($error instanceof ConnectionException){
            response()->httpCode(500)->json(['error' => $error->getMessage()]);
            return;
        } else {
            response()->httpCode(500)->json(['error' => $error->getMessage()]);
            return;
        }

        throw $error;
    }
}
