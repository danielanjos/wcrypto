<?php

function encrypt($value)
{
    return openssl_encrypt(
        json_encode($value),
        'AES-128-CBC',
        pack('a16', KEY_HASH_01),
        0,
        pack('a16', KEY_HASH_02)
    );
}

function decrypt($value)
{
    return openssl_decrypt(
        json_encode($value),
        'AES-128-CBC',
        pack('a16', KEY_HASH_01),
        0,
        pack('a16', KEY_HASH_02)
    );
}
