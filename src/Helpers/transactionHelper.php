<?php

use DanielAnjos\WCrypto\Http\Entities\Deposit;

function getAllowedTransactions()
{
    return [
        'Deposit',
        'Withdrawal',
        'Transfer'
    ];
}
