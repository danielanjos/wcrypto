<?php

namespace DanielAnjos\WCrypto\Http\Contracts;

use DanielAnjos\WCrypto\Http\DAOs\AccountDAO;
use DanielAnjos\WCrypto\Http\Entities\Account;

interface AccountsRepositoryInterface
{
    public function __construct(AccountDAO $accountDAO);

    public function create(array $data): Account;

    public function show(string $accoun_number): Account;
}
