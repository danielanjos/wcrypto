<?php

namespace DanielAnjos\WCrypto\Http\Contracts;

use DanielAnjos\WCrypto\Http\DAOs\SessionsDAO;

interface SessionsRepositoryInterface
{
    public function __construct(SessionsDAO $sessionsDAO);

    public function signIn(array $data);

}
