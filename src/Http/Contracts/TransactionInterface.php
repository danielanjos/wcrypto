<?php

namespace DanielAnjos\WCrypto\Http\Contracts;

/**
 * @property int $id
 * @property string $type
 * @property \DanielAnjos\WCrypto\Http\Entities\Account $source_account
 * @property float $value
 * @property \DanielAnjos\WCrypto\Http\Entities\Account $target_account
 */
interface TransactionInterface
{
    public function setId($id);
}
