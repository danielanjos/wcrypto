<?php

namespace DanielAnjos\WCrypto\Http\Contracts;

interface TransactionsRepositoryInterface
{
    public function create(array $data);

    public function validateDeposit(TransactionInterface $transaction);

    public function validateWithdrawal(TransactionInterface $transaction);

    public function validateTransfer(TransactionInterface $transaction);

}
