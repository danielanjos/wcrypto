<?php

namespace DanielAnjos\WCrypto\Http\Contracts;

use DanielAnjos\WCrypto\Http\DAOs\UserDAO;
use DanielAnjos\WCrypto\Http\Entities\User;
use stdClass;

interface UsersRepositoryInterface
{
    public function __construct(UserDAO $userDAO);

    public function create(array $data): User;
}
