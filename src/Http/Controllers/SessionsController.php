<?php

namespace DanielAnjos\WCrypto\Http\Controllers;

use DanielAnjos\WCrypto\Http\Contracts\SessionsRepositoryInterface;

class SessionsController
{
    /**
     * @Inject
     * @var SessionsRepositoryInterface
     */
    private $sessionsRepository;

    public function __construct(
        SessionsRepositoryInterface $sessionsRepository

    ) {
        $this->sessionsRepository = $sessionsRepository;
    }

    public function authenticate()
    {

        $data = input()->all();

        $response = $this->sessionsRepository->signIn($data);

        return response()->json($response);
    }

    public function teste(){
        echo 'teste';

        exit;
    }
}
