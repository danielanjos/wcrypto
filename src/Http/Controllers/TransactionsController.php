<?php

namespace DanielAnjos\WCrypto\Http\Controllers;

use DanielAnjos\WCrypto\Http\Contracts\TransactionsRepositoryInterface;
use DanielAnjos\WCrypto\Http\Entities\Deposit;

class TransactionsController
{

    /**
     * @Inject
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @Inject
     * @var TransactionsRepositoryInterface
     */
    private $transactionsRepository;

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        TransactionsRepositoryInterface $transactionsRepository
    ) {
        $this->logger = $logger;
        $this->transactionsRepository = $transactionsRepository;
    }

    public function create()
    {
        $data = input()->all();

        /**
         * @var \DanielAnjos\WCrypto\Http\Contracts\TransactionInterface $transaction
         */
        $transaction = $this->transactionsRepository->create($data);

        $this->logger->info(
            $transaction->type,
            [
                'source_account' => $transaction->source_account,
                'value' => $transaction->value
            ]
        );

        return response()->httpCode(200)->json([
            'transaction' => $transaction
        ]);
    }
}
