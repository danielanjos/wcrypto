<?php

namespace DanielAnjos\WCrypto\Http\Controllers;

use DanielAnjos\WCrypto\Http\Contracts\AccountsRepositoryInterface;
use DanielAnjos\WCrypto\Http\Contracts\UsersRepositoryInterface;

class UsersController
{

    /**
     * @Inject
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @Inject
     * @var UsersRepositoryInterface
     */
    private $usersRepository;

    /**
     * @Inject
     * @var AccountsRepositoryInterface
     */
    private $accountsRepository;

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        UsersRepositoryInterface $usersRepository,
        AccountsRepositoryInterface $accountsRepository
    ) {
        $this->logger = $logger;
        $this->usersRepository = $usersRepository;
        $this->accountsRepository = $accountsRepository;
    }

    public function create()
    {
        $data = input()->all();

        $user = $this->usersRepository->create($data);

        $this->logger->info(
            'User created',
            ['user_id' => $user->id,]
        );

        $data["user"] = $user;
        $account = $this->accountsRepository->create($data);

        $this->logger->info(
            'Account created',
            ['account_id' => $account->id]
        );

        return response()->httpCode(201)->json([
            'account' => $account
        ]);
    }
}
