<?php

namespace DanielAnjos\WCrypto\Http\DAOs;

use DanielAnjos\WCrypto\Exceptions\AuthorizationException;
use DanielAnjos\WCrypto\Http\Entities\Account;
use DanielAnjos\WCrypto\Http\Entities\User;
use PDOException;

class AccountDAO extends DAO
{
    public function insert(Account $account)
    {
        $query = "INSERT INTO accounts (account_number, balance, user_id)
            VALUES (:account_number, :balance, :user_id)";

        $statement = $this->pdo->prepare($query);
        $statement->bindValue(':account_number', $account->account_number);
        $statement->bindValue(':balance', $account->balance);
        $statement->bindValue(':user_id', $account->user->id);

        if (!$statement->execute()) {
            throw new PDOException("Error " . $statement->errorCode() . " contact support");
        }

        $account->setId($this->pdo->lastInsertId());

        return $account;
    }

    public function getAccountByNumber(string $account_number): Account
    {
        $query = "SELECT a.id, account_number, balance, user_id, u.name, u.cpf_cnpj, u.rg_ie, u.birth_date, u.phone, u.address, u.address_number, u.city, u.country
            FROM accounts a
            JOIN users u ON u.id = a.user_id
            WHERE account_number = ?";

        $statement = $this->pdo->prepare($query);
        $statement->bindValue(1, $account_number);
        $statement->execute();

        $accountData = $statement->fetch();

        if(!$accountData)
        {
            throw new AuthorizationException();
        }

        $user = new User($accountData);

        $accountData["user"] = $user;

        return new Account($accountData);
    }
}
