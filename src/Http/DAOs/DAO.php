<?php

namespace DanielAnjos\WCrypto\Http\DAOs;

use DanielAnjos\WCrypto\Database\Connection;

/**
 * @property \PDO $pdo
 */
class DAO
{
    protected $pdo;

    public function __construct($pdo = null)
    {
        $this->pdo = $pdo instanceof \PDO ? $pdo : Connection::connect();
    }

    public function __destruct()
    {
        Connection::closeConnection($this->pdo);
    }
}
