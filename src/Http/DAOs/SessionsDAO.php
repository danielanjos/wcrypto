<?php

namespace DanielAnjos\WCrypto\Http\DAOs;

use DanielAnjos\WCrypto\Http\Entities\Account;
use DanielAnjos\WCrypto\Http\Entities\User;
use InvalidArgumentException;

class SessionsDAO extends DAO
{
    public function getUser(string $document): User
    {
        $query = "SELECT
                id,
                name,
                cpf_cnpj,
                rg_ie,
                birth_date, phone,
                password,
                address,
                address_number,
                city,
                country
            FROM users
            WHERE cpf_cnpj = ?";

        $statement = $this->pdo->prepare($query);
        $statement->bindValue(1, $document);
        $statement->execute();

        $userData = $statement->fetch();

        if (!$userData) {
            throw new InvalidArgumentException('Incorrect user or password');
        }

        return new User($userData);
    }

    public function getAccountByUserId(User $user)
    {
        $query = "SELECT a.id, account_number, balance
            FROM accounts a
            WHERE a.user_id = ?";

        $statement = $this->pdo->prepare($query);
        $statement->bindValue(1, $user->id);
        $statement->execute();

        $accountData = $statement->fetch();

        $accountData["user"] = $user;

        return new Account($accountData);
    }
}
