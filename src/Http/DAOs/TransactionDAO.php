<?php

namespace DanielAnjos\WCrypto\Http\DAOs;

use DanielAnjos\WCrypto\Http\Contracts\TransactionInterface;
use PDOException;

class TransactionDAO extends DAO
{
    public function insert(TransactionInterface $transaction)
    {
        $query = "INSERT INTO transactions (source_account_id, type, value, target_account_id)
            VALUES (:source_account_id, :type, :value, :target_account_id);";

        $statement = $this->pdo->prepare($query);
        $statement->bindValue(':source_account_id', $transaction->source_account->id);
        $statement->bindValue(':type', $transaction->type);
        $statement->bindValue(':value', $transaction->value);
        $statement->bindValue(':target_account_id', $transaction->target_account->id);

        if (!$statement->execute()) {
            var_dump($statement->errorInfo());
            throw new PDOException("Error " . $statement->errorCode() . " contact support");
        }

        $transaction->setId($this->pdo->lastInsertId());

        $typeLowerCase = strtolower($transaction->type);

        $this->$typeLowerCase($transaction);
    }

    private function deposit(TransactionInterface $transaction)
    {
        $query = "UPDATE accounts SET balance = balance + :value WHERE id = :target_account";

        $statement = $this->pdo->prepare($query);
        $statement->bindValue(':target_account', $transaction->target_account->id);
        $statement->bindValue(':value', $transaction->value);

        if (!$statement->execute()) {
            throw new PDOException("Error " . $statement->errorCode() . " contact support");
        }

        if($transaction->type === 'Deposit')
        {
            $transaction->source_account->setBalance(
                $transaction->source_account->balance + $transaction->value
            );
        } else {
            $transaction->target_account->setBalance(
                $transaction->target_account->balance + $transaction->value
            );
        }

        // echo 'Deposit' . $transaction->target_account->account_number . PHP_EOL;
    }

    private function withdrawal(TransactionInterface $transaction)
    {
        $query = "UPDATE accounts SET balance = balance - :value WHERE id = :source_account";

        $statement = $this->pdo->prepare($query);
        $statement->bindValue(':source_account', $transaction->source_account->id);
        $statement->bindValue(':value', $transaction->value);

        if (!$statement->execute()) {
            throw new PDOException("Error " . $statement->errorCode() . " contact support");
        }

        $transaction->source_account->setBalance(
            $transaction->source_account->balance - $transaction->value
        );

        // echo 'Withdrawal ' . $transaction->source_account->account_number . PHP_EOL;
    }

    private function transfer(TransactionInterface $transaction)
    {
        $this->withdrawal($transaction);

        $this->deposit($transaction);
    }
}
