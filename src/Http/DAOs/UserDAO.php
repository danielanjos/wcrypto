<?php

namespace DanielAnjos\WCrypto\Http\DAOs;

use DanielAnjos\WCrypto\Http\Entities\User;
use PDOException;

class UserDAO extends DAO
{
    public function documentExists(string $document)
    {
        $query = "SELECT COUNT(*) FROM users WHERE cpf_cnpj = ?";

        $statement = $this->pdo->prepare($query);
        $statement->bindValue(1, $document);
        $statement->execute();

        $userCount = $statement->fetch();

        return (bool)$userCount[0];
    }

    public function insert(User $user): User
    {
        $query = "INSERT INTO users (
                name,
                cpf_cnpj,
                rg_ie,
                birth_date,
                phone,
                password,
                address,
                address_number,
                city,
                country
            ) VALUES (
                :name,
                :cpf_cnpj,
                :rg_ie,
                :birth_date,
                :phone,
                :password,
                :address,
                :address_number,
                :city,
                :country)";

        $statement = $this->pdo->prepare($query);
        $statement->bindValue(':name', $user->name);
        $statement->bindValue(':cpf_cnpj', $user->cpf_cnpj);
        $statement->bindValue(':rg_ie', $user->rg_ie);
        $statement->bindValue(':birth_date', $user->birth_date);
        $statement->bindValue(':phone', $user->phone);
        $statement->bindValue(':password', $user->password);
        $statement->bindValue(':address', $user->address);
        $statement->bindValue(':address_number', $user->address_number);
        $statement->bindValue(':city', $user->city);
        $statement->bindValue(':country', $user->country);

        if (!$statement->execute()) {
            throw new PDOException("Error " . $statement->errorCode() . " contact support");
        }

        $user->setId($this->pdo->lastInsertId());

        return $user;
    }
}
