<?php

namespace DanielAnjos\WCrypto\Http\Entities;

use JsonSerializable;

/**
 * @property int $id
 * @property string $account_number
 * @property float $balance
 * @property User $user
 */
class Account implements JsonSerializable
{
    private $id;
    private $account_number;
    private $balance;
    private $user;

    public function __construct(array $data)
    {
        $this->setAttributes($data);
    }

    private function setAttributes(array $data)
    {
        foreach ($data as $attributeName => $attributeValue) {
            if (property_exists($this, $attributeName)) {
                $this->$attributeName = $attributeValue;
            }
        }
    }

    function __get($name)
    {
        return $this->$name;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'account_number' => $this->account_number,
            'balance' => $this->balance,
            'user' => [
                'id' => $this->user->id,
                'name' => $this->user->name,
                'cpf_cnpj' => $this->user->cpf_cnpj,
                'rg_ie' => $this->user->rg_ie,
                'birth_date' => $this->user->birth_date,
                'phone' => $this->user->phone,
                'password' => $this->user->password,
                'address' => $this->user->address,
                'address_number' => $this->user->address_number,
                'city' => $this->user->city,
                'country' => $this->user->country,
            ]
        ];
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set the value of balance
     *
     * @return  self
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;

        return $this;
    }
}
