<?php

namespace DanielAnjos\WCrypto\Http\Entities;

use DanielAnjos\WCrypto\Http\Contracts\TransactionInterface;
use JsonSerializable;

/**
 * @property int $id
 * @property string $type
 * @property \DanielAnjos\WCrypto\Http\Entities\Account $source_account
 * @property float $value
 * @property \DanielAnjos\WCrypto\Http\Entities\Account $target_account
 */
class Transaction implements TransactionInterface, JsonSerializable
{
    private $id;
    private $type;
    private $source_account;
    private $value;
    private $target_account;

    public function __construct(array $data)
    {
        $this->setAttributes($data);
    }

    private function setAttributes(array $data)
    {
        foreach ($data as $attributeName => $attributeValue) {
            if (property_exists($this, $attributeName)) {
                $this->$attributeName = $attributeValue;
            }
        }
    }

    function __get($name)
    {
        return $this->$name;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'source_account' => $this->source_account,
            'value' => $this->value,
            'target_account' => $this->target_account,
        ];
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}
