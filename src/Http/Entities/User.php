<?php

namespace DanielAnjos\WCrypto\Http\Entities;

use JsonSerializable;

/**
 * @property int $id
 * @property string $name
 * @property string $cpf_cnpj
 * @property string $rg_ie
 * @property string $birth_date
 * @property string $phone
 * @property string $password
 * @property string $address
 * @property int $address_number
 * @property string $city
 * @property string $country
 */
class User implements JsonSerializable
{
    private $id;
    private $name;
    private $cpf_cnpj;
    private $rg_ie;
    private $birth_date;
    private $phone;
    private $password;
    private $address;
    private $address_number;
    private $city;
    private $country;

    public function __construct(array $data)
    {
        $this->setAttributes($data);
    }

    private function setAttributes(array $data)
    {
        foreach ($data as $attributeName => $attributeValue) {
            if (property_exists($this, $attributeName)) {
                $this->$attributeName = $attributeValue;
            }
        }
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'cpf_cnpj' => $this->cpf_cnpj,
            'rg_ie' => $this->rg_ie,
            'birth_date' => $this->birth_date,
            'phone' => $this->phone,
            'password' => $this->password,
            'address' => $this->address,
            'address_number' => $this->address_number,
            'city' => $this->city,
            'country' => $this->country,
        ];
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}
