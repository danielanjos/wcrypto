<?php

namespace DanielAnjos\WCrypto\Http\Middlewares;

use DanielAnjos\WCrypto\Exceptions\AuthorizationException;
use DanielAnjos\WCrypto\Http\Contracts\AccountsRepositoryInterface;
use Firebase\JWT\JWT;
use Pecee\Http\Middleware\IMiddleware;
use Pecee\Http\Request;

class Authenticate implements IMiddleware
{
    /**
     * @Inject
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @Inject
     * @var AccountsRepositoryInterface
     */
    private $accountsRepository;

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        AccountsRepositoryInterface $accountsRepository
    ) {
        $this->logger = $logger;
        $this->accountsRepository = $accountsRepository;
    }


    public function handle(Request $request): void
    {
        $authorizationHeader = $request->getHeader('http_authorization');

        if (!$authorizationHeader) {
            throw new AuthorizationException();
        }

        $token = str_replace('Bearer ', '', $authorizationHeader);

        $data = JWT::decode($token, JWT_KEY, ['HS256']);

        $account_number = $data->account_number;

        $account = $this->accountsRepository->show($account_number);

        $request->authenticated = true;

        $this->logger->info('Authenticate user', [
            'url' => $request->getUrl()->getOriginalUrl(),
            'user_id' => $account->user->id
        ]);

    }
}
