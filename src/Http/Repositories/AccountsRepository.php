<?php

namespace DanielAnjos\WCrypto\Http\Repositories;

use DanielAnjos\WCrypto\Http\Contracts\AccountsRepositoryInterface;
use DanielAnjos\WCrypto\Http\DAOs\AccountDAO;
use DanielAnjos\WCrypto\Http\Entities\Account;

class AccountsRepository implements AccountsRepositoryInterface
{
    private $dao;

    public function __construct(AccountDAO $accountDAO)
    {
        $this->dao = $accountDAO;
    }

    public function create(array $data): Account
    {
        $data["account_number"] = bin2hex(random_bytes(5));

        $this->encryptData($data);

        $newAccount = new Account($data);

        $account = $this->dao->insert($newAccount);

        return $account;
    }

    private function encryptData(&$data)
    {
        foreach ($data as $key => $value) {

            if (
                $key === 'user'
                || $key === 'balance'
            ) continue;

            $openssl = encrypt($value);

            $data[$key] = $openssl;
        }
    }

    public function show(string $accoun_number): Account
    {
        return $this->dao->getAccountByNumber($accoun_number);
    }
}
