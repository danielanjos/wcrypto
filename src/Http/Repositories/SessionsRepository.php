<?php

namespace DanielAnjos\WCrypto\Http\Repositories;

use DanielAnjos\WCrypto\Http\Contracts\SessionsRepositoryInterface;
use DanielAnjos\WCrypto\Http\DAOs\SessionsDAO;
use DanielAnjos\WCrypto\Http\Entities\Account;
use Firebase\JWT\JWT;
use InvalidArgumentException;

class SessionsRepository implements SessionsRepositoryInterface
{
    private $dao;

    public function __construct(SessionsDAO $sessionsDAO)
    {
        $this->dao = $sessionsDAO;
    }

    public function signIn(array $data)
    {
        extract($data);

        $clearedDocument = onlyNumbersData($document);

        $encryptedDocument = encrypt($clearedDocument);

        $user = $this->dao->getUser($encryptedDocument);

        $this->checkPassword($password, $user->password);

        $account = $this->dao->getAccountByUserId($user);

        $token = $this->generateToken($account);

        $response = $this->configureResponse($account, $token);

        return $response;
    }

    private function configureResponse(Account $account, string $token)
    {
        $response = [
            'account' => $account,
            'token' => $token
        ];

        return $response;
    }

    private function generateToken(Account $account)
    {
        $token = JWT::encode(
            [
                'user_id' => $account->user->id,
                'name' => $account->user->name,
                'cpf_cnpj' => $account->user->cpf_cnpj,
                'account_number' => $account->account_number
            ],
            JWT_KEY
        );

        return 'Bearer ' . $token;
    }

    private function checkPassword(string $password, string $hash)
    {
        if (!password_verify($password, $hash)) {
            throw new InvalidArgumentException('Incorrect user or password');
        }
    }
}
