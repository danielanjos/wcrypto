<?php

namespace DanielAnjos\WCrypto\Http\Repositories;

use DanielAnjos\WCrypto\Http\Contracts\TransactionInterface;
use DanielAnjos\WCrypto\Http\Contracts\TransactionsRepositoryInterface;
use DanielAnjos\WCrypto\Http\DAOs\AccountDAO;
use DanielAnjos\WCrypto\Http\DAOs\TransactionDAO;
use DanielAnjos\WCrypto\Http\Entities\Transaction;
use Exception;
use InvalidArgumentException;

class TransactionsRepository implements TransactionsRepositoryInterface
{
    private $dao;
    private $accountDAO;

    public function __construct(TransactionDAO $transactionDAO, AccountDAO $accountDAO)
    {
        $this->dao = $transactionDAO;
        $this->accountDAO = $accountDAO;
    }

    public function create(array $data)
    {
        $allowedTransactions = getAllowedTransactions();

        $source_account = $this
            ->accountDAO
            ->getAccountByNumber($data["source_account_number"]);

        $value = $data["value"];
        $type = $data["type"];

        $target_account = isset($data["target_account_number"]) ?
            $this->accountDAO->getAccountByNumber($data["target_account_number"]) :
            $source_account;

        if (!in_array($type, $allowedTransactions)) {
            throw new Exception('Invalid transaction');
        };

        $transaction = new Transaction(
            [
                'type' => $type,
                'source_account' => $source_account,
                'value' => $value,
                'target_account' => $target_account
            ]
        );

        $validateMethod = 'validate' . $transaction->type;

        $this->$validateMethod($transaction);

        $this->dao->insert($transaction);

        return $transaction;
    }

    public function validateDeposit(TransactionInterface $transaction)
    {
        return;
    }

    public function validateWithdrawal(TransactionInterface $transaction)
    {
        if ($transaction->value > $transaction->source_account->balance) {
            throw new InvalidArgumentException('Insufficient balance');
        }
    }

    public function validateTransfer(TransactionInterface $transaction)
    {
        if ($transaction->value > $transaction->source_account->balance) {
            throw new InvalidArgumentException('Insufficient balance');
        }
    }
}
