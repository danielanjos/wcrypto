<?php

namespace DanielAnjos\WCrypto\Http\Repositories;

use DanielAnjos\WCrypto\Http\Contracts\UsersRepositoryInterface;
use DanielAnjos\WCrypto\Http\DAOs\UserDAO;
use DanielAnjos\WCrypto\Http\Entities\User;
use Exception;

class UsersRepository implements UsersRepositoryInterface
{
    private $dao;

    public function __construct(UserDAO $userDAO)
    {
        $this->dao = $userDAO;
    }

    public function create(array $data): User
    {
        $data["cpf_cnpj"] = onlyNumbersData($data["cpf_cnpj"]);

        $this->encryptData($data);

        if($this->dao->documentExists($data["cpf_cnpj"]))
        {
            throw new Exception('An account already exists with this document');
        }

        $data["password"] = password_hash($data["password"], PASSWORD_DEFAULT);

        $newUser = new User($data);

        $user = $this->dao->insert($newUser);

        return $user;
    }

    private function encryptData(&$data)
    {
        foreach ($data as $key => $value) {

            if ($key === 'birth_date' || $key === 'password') continue;

            $openssl = encrypt($value);

            $data[$key] = $openssl;
        }
    }
}
