<?php

namespace DanielAnjos\WCrypto;

use DI\Container;
use Pecee\SimpleRouter\ClassLoader\IClassLoader;
use Pecee\SimpleRouter\SimpleRouter;

class Router extends SimpleRouter
{
    /**
     * @throws \Exception
     * @throws \Pecee\Http\Middleware\Exceptions\TokenMismatchException
     * @throws \Pecee\SimpleRouter\Exceptions\HttpException
     * @throws \Pecee\SimpleRouter\Exceptions\NotFoundHttpException
     */
    public static function start(): void
    {
        // Load our helpers
        require_once 'Helpers/routerHelper.php';

        // Load our custom routes
        require_once '../routes/api.php';

        parent::setDefaultNamespace(DEFAULT_NAMESPACE);

        // Do initial stuff
        parent::start();
    }
}
