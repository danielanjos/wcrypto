<?php

namespace DanielAnjos\WCrypto\Tests\Database;

use DanielAnjos\WCrypto\Database\Connection;
use PDO;
use PHPUnit\Framework\TestCase;

class DatabaseTest extends TestCase
{

    public static function createTables()
    {
        $pdo = self::createConnection();

        $content = file_get_contents('tables.sql');

        $pdo->exec($content);
    }

    public  static function createDatabase()
    {
        $pdo = new \Pdo(
            DB_CONNECTION_TEST .
                ":host=" . DB_HOST_TEST,
            DB_USERNAME_TEST,
            DB_PASSWORD_TEST,
            array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")
        );

        $pdo->exec("CREATE DATABASE IF NOT EXISTS " . DB_DBNAME_TEST)
            or die(var_dump($pdo->errorInfo()));
    }

    public static function createConnection()
    {
        $connection = new \PDO(
            DB_CONNECTION_TEST .
                ":host=" . DB_HOST_TEST .
                ";dbname=" . DB_DBNAME_TEST,
            DB_USERNAME_TEST,
            DB_PASSWORD_TEST,
            array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")
        );

        return $connection;
    }

    /**
     * @test
     */
    public function shouldBeAbleToConnectInDatabaseWhenDataIsCorrect()
    {
        $pdo = Connection::connect();

        $this->assertInstanceOf(PDO::class, $pdo);
    }
}
