<?php

namespace DanielAnjos\WCrypto\Tests\Helpers;

use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class EncryptHelperTest extends TestCase
{
    /**
     * @test
     * @dataProvider encryptDataProvider
     */
    public function shouldBeAbleToEncryptDataWhenIsCalled($data, $expctedResult)
    {
        $encryptedData = encrypt($data);

        $this->assertEquals($expctedResult, $encryptedData);
    }

    /**
     * @test
     * @dataProvider decryptDataProvider
     */
    public function shouldBeAbleToDecryptDataWhenIsCalled($data, $expctedResult)
    {
        $encryptedData = decrypt($data);

        $this->assertEquals($expctedResult, $encryptedData);
    }

    public function encryptDataProvider()
    {
        return [
            'shouldBeAbleToEncryptWhenDataIsNumber' => [
                'data' => 123,
                'expectedResult' => '+GJ5O8+X4Kq3jwuIMEMiqg=='
            ],
            'shouldBeAbleToEncryptWhenDataIsStringNumber' => [
                'data' => '123',
                'expectedResult' => 'x8PBVtxHYBd7zpsfGVw/hw=='
            ],
            'shouldBeAbleToEncryptWhenDataIsString' => [
                'data' => 'Lorem Ipsum',
                'expectedResult' => 'uN9HzaIz0/AHQIltJuWagg=='
            ]
        ];
    }

    public function decryptDataProvider()
    {
        return [
            'shouldBeAbleToDecryptWhenDataWasNumber' => [
                'data' => '+GJ5O8+X4Kq3jwuIMEMiqg==',
                'expectedResult' => 123
            ],
            'shouldBeAbleToDecryptWhenDataWasStringNumber' => [
                'data' => 'x8PBVtxHYBd7zpsfGVw/hw==',
                'expectedResult' => '"123"'
            ],
            'shouldBeAbleToDecryptWhenDataWasString' => [
                'data' => 'uN9HzaIz0/AHQIltJuWagg==',
                'expectedResult' => '"Lorem Ipsum"'
            ]
        ];
    }
}
