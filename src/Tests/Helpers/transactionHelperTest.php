<?php

namespace DanielAnjos\WCrypto\Tests\Helpers;

use PHPUnit\Framework\TestCase;

class TransactionHelperTest extends TestCase
{
    /**
     * @test
     */
    public function shouldBeAbleToShowAllowedTransactionsWhenIsCalled()
    {
        $expctedResult = [
            'Deposit',
            'Withdrawal',
            'Transfer'
        ];

        $allowedTransactions = getAllowedTransactions();

        $this->assertEquals($expctedResult, $allowedTransactions);
    }

}
