<?php

namespace DanielAnjos\WCrypto\Tests\Http\DAOs;

use DanielAnjos\WCrypto\Http\DAOs\AccountDAO;
use DanielAnjos\WCrypto\Http\DAOs\UserDAO;
use DanielAnjos\WCrypto\Http\Entities\Account;
use DanielAnjos\WCrypto\Http\Entities\User;
use DanielAnjos\WCrypto\Tests\Database\DatabaseTest;
use PDOException;
use PHPUnit\Framework\TestCase;

class AccountDAOTest extends TestCase
{
    private static $pdo;

    public static function setUpBeforeClass(): void
    {
        DatabaseTest::createDatabase();

        self::$pdo = DatabaseTest::createConnection();

        DatabaseTest::createTables();
    }

    private function createUser()
    {
        $data = [
            'name' => 'John Doe',
            'cpf_cnpj' => '999',
            'rg_ie' => '999',
            'birth_date' => '2020-09-09',
            'phone' => '9912345678',
            'password' => '123',
            'address' => 'Lorem Ipsum Address',
            'address_number' => 1,
            'city' => 'Lorem Ipsum City',
            'country' => 'Lorem Ipsum Country',
        ];

        $user = new User($data);

        $query = "INSERT INTO users (
            name,
            cpf_cnpj,
            rg_ie,
            birth_date,
            phone,
            password,
            address,
            address_number,
            city,
            country
        ) VALUES (
            :name,
            :cpf_cnpj,
            :rg_ie,
            :birth_date,
            :phone,
            :password,
            :address,
            :address_number,
            :city,
            :country)";

        $statement = self::$pdo->prepare($query);
        $statement->bindValue(':name', $user->name);
        $statement->bindValue(':cpf_cnpj', $user->cpf_cnpj);
        $statement->bindValue(':rg_ie', $user->rg_ie);
        $statement->bindValue(':birth_date', $user->birth_date);
        $statement->bindValue(':phone', $user->phone);
        $statement->bindValue(':password', $user->password);
        $statement->bindValue(':address', $user->address);
        $statement->bindValue(':address_number', $user->address_number);
        $statement->bindValue(':city', $user->city);
        $statement->bindValue(':country', $user->country);

        $statement->execute();

        $user->setId(self::$pdo->lastInsertId());

        return $user;
    }

    /**
     * @test
     */
    public function shouldBeAbleToInsertANewAccountWhenCreatedUser()
    {
        $user = $this->createUser();

        $data = [
            'account_number' => '777',
            'balance' => 1000,
            'user' => $user
        ];


        $account = new Account($data);

        $accountDAO = new AccountDAO(self::$pdo);

        $returnedAccount = $accountDAO->insert($account);

        $this->objectHasAttribute('id', $returnedAccount);
        $this->assertEquals(1000, $returnedAccount->balance);
    }

    /**
     * @test
     */
    public function shouldNotBeAbleToInsertANewAccountWhenUserIsNotExists()
    {
        $data = [
            'account_number' => '777',
            'balance' => 1000,
            'user' => new User(['id' => 0])
        ];

        $account = new Account($data);

        $accountDAO = new AccountDAO(self::$pdo);

        $this->expectException(PDOException::class);
        $accountDAO->insert($account);
    }

    public static function tearDownAfterClass(): void
    {
        self::$pdo->exec('DROP DATABASE ' . DB_DBNAME_TEST);
    }
}
