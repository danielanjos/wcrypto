<?php

namespace DanielAnjos\WCrypto\Tests\Http\DAOs;

use DanielAnjos\WCrypto\Http\DAOs\AccountDAO;
use DanielAnjos\WCrypto\Http\DAOs\TransactionDAO;
use DanielAnjos\WCrypto\Http\Entities\Account;
use DanielAnjos\WCrypto\Http\Entities\Transaction;
use DanielAnjos\WCrypto\Http\Entities\User;
use DanielAnjos\WCrypto\Tests\Database\DatabaseTest;
use PDOException;
use PHPUnit\Framework\TestCase;

/**
 * @property Account source_account
 * @property Account target_account
 */
class TransactionDAOTest extends TestCase
{
    private static $pdo;
    private static $accounts;

    public static function setUpBeforeClass(): void
    {
        DatabaseTest::createDatabase();

        self::$pdo = DatabaseTest::createConnection();

        DatabaseTest::createTables();

        self::$accounts = self::createAccounts();
    }

    private static function createAccounts()
    {
        $usersData = [
            [
                'name' => 'John Doe',
                'cpf_cnpj' => '999',
                'rg_ie' => '999',
                'birth_date' => '2020-09-09',
                'phone' => '9912345678',
                'password' => '123',
                'address' => 'Lorem Ipsum Address',
                'address_number' => 1,
                'city' => 'Lorem Ipsum City',
                'country' => 'Lorem Ipsum Country',
            ],
            [
                'name' => 'Other John Doe',
                'cpf_cnpj' => '555',
                'rg_ie' => '555',
                'birth_date' => '2020-09-09',
                'phone' => '0012345678',
                'password' => '123',
                'address' => 'Lorem Ipsum Address',
                'address_number' => 2,
                'city' => 'Lorem Ipsum City',
                'country' => 'Lorem Ipsum Country',
            ]
        ];

        $accountsData = [
            [
                'account_number' => '111',
                'balance' => 1000,
                'user' => self::createUsers($usersData[0])
            ],
            [
                'account_number' => '222',
                'balance' => 5000,
                'user' => self::createUsers($usersData[1])
            ]
        ];

        $accounts = [
            new Account($accountsData[0]),
            new Account($accountsData[1])
        ];

        return [
            'source_account' => self::inserAccount($accounts[0]),
            'target_account' => self::inserAccount($accounts[1])
        ];
    }

    private static function inserAccount(Account $account)
    {
        $query = "INSERT INTO accounts (account_number, balance, user_id)
            VALUES (:account_number, :balance, :user_id)";

        $statement = self::$pdo->prepare($query);
        $statement->bindValue(':account_number', $account->account_number);
        $statement->bindValue(':balance', $account->balance);
        $statement->bindValue(':user_id', $account->user->id);

        if (!$statement->execute()) {
            var_dump($statement->errorInfo());
            throw new PDOException("Error " . $statement->errorCode() . " contact support");
        }

        $account->setId(self::$pdo->lastInsertId());

        return $account;
    }

    private static function createUsers($data)
    {
        $user = new User($data);

        $query = "INSERT INTO users (
            name,
            cpf_cnpj,
            rg_ie,
            birth_date,
            phone,
            password,
            address,
            address_number,
            city,
            country
        ) VALUES (
            :name,
            :cpf_cnpj,
            :rg_ie,
            :birth_date,
            :phone,
            :password,
            :address,
            :address_number,
            :city,
            :country)";

        $statement = self::$pdo->prepare($query);
        $statement->bindValue(':name', $user->name);
        $statement->bindValue(':cpf_cnpj', $user->cpf_cnpj);
        $statement->bindValue(':rg_ie', $user->rg_ie);
        $statement->bindValue(':birth_date', $user->birth_date);
        $statement->bindValue(':phone', $user->phone);
        $statement->bindValue(':password', $user->password);
        $statement->bindValue(':address', $user->address);
        $statement->bindValue(':address_number', $user->address_number);
        $statement->bindValue(':city', $user->city);
        $statement->bindValue(':country', $user->country);

        $statement->execute();

        $user->setId(self::$pdo->lastInsertId());

        return $user;
    }

    /**
     * @test
     */
    public function shouldBeAbleToInsertDeposit()
    {

        $data = [
            'source_account' => self::$accounts['source_account'],
            'type' => 'Deposit',
            'value' => 500,
            'target_account' => self::$accounts['source_account']
        ];


        $transaction = new Transaction($data);

        $transactionDAO = new TransactionDAO(self::$pdo);

        $transactionDAO->insert($transaction);

        $this->assertEquals(1500, $transaction->source_account->balance);
    }

    /**
     * @test
     */
    public function shouldBeAbleToInsertWithdrawal()
    {
        $data = [
            'source_account' => self::$accounts['source_account'],
            'type' => 'Withdrawal',
            'value' => 500,
            'target_account' => self::$accounts['source_account']
        ];

        $transaction = new Transaction($data);

        $transactionDAO = new TransactionDAO(self::$pdo);

        $transactionDAO->insert($transaction);

        $this->assertEquals(1000, $transaction->source_account->balance);
    }

    /**
     * @test
     */
    public function shouldBeAbleToInserTransfer()
    {
        $data = [
            'source_account' => self::$accounts['source_account'],
            'type' => 'Transfer',
            'value' => 500,
            'target_account' => self::$accounts['target_account']
        ];

        $transaction = new Transaction($data);

        $transactionDAO = new TransactionDAO(self::$pdo);

        $transactionDAO->insert($transaction);

        $this->assertEquals(500, $transaction->source_account->balance);
        $this->assertEquals(5500, $transaction->target_account->balance);
    }



    public static function tearDownAfterClass(): void
    {
        self::$pdo->exec('DROP DATABASE ' . DB_DBNAME_TEST);
    }
}
