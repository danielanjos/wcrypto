<?php

namespace DanielAnjos\WCrypto\Tests\Http\DAOs;

use DanielAnjos\WCrypto\Http\DAOs\UserDAO;
use DanielAnjos\WCrypto\Http\Entities\User;
use DanielAnjos\WCrypto\Tests\Database\DatabaseTest;
use PDOException;
use PHPUnit\Framework\TestCase;

class UserDAOTest extends TestCase
{
    private static $pdo;

    public static function setUpBeforeClass(): void
    {
        DatabaseTest::createDatabase();

        self::$pdo = DatabaseTest::createConnection();

        DatabaseTest::createTables();
    }

    /**
     * @test
     * @dataProvider userInsertDataProvider
     */
    public function shouldBeAbleToInserANewUserWhenDocumentNotExists($data)
    {
        $user = new User($data);

        $userDAO = new UserDAO(self::$pdo);

        $returnedUser = $userDAO->insert($user);

        $this->objectHasAttribute('id', $returnedUser);
        $this->assertEquals('999', $returnedUser->cpf_cnpj);
    }

    /**
     * @test
     * @dataProvider userInsertDataProvider
     * @dataProvider userIncorrectDataProvider
     */
    public function shouldNotBeAbleToInserANewUserWhenDataIsIncorrect($data)
    {
        $user = new User($data);

        $userDAO = new UserDAO(self::$pdo);

        $this->expectException(PDOException::class);
        $userDAO->insert($user);
    }

    public function userInsertDataProvider()
    {
        return [
            'userData' => [
                'data' => [
                    'name' => 'John Doe',
                    'cpf_cnpj' => '999',
                    'rg_ie' => '999',
                    'birth_date' => '2020-09-09',
                    'phone' => '9912345678',
                    'password' => '123',
                    'address' => 'Lorem Ipsum Address',
                    'address_number' => 1,
                    'city' => 'Lorem Ipsum City',
                    'country' => 'Lorem Ipsum Country',
                ]
            ]
        ];
    }

    public function userIncorrectDataProvider()
    {
        return [
            'whenNameIsEmpty' => [
                'data' => [
                    'cpf_cnpj' => '999',
                    'rg_ie' => '999',
                    'birth_date' => '2020-09-09',
                    'phone' => '9912345678',
                    'password' => '123',
                    'address' => 'Lorem Ipsum Address',
                    'address_number' => 1,
                    'city' => 'Lorem Ipsum City',
                    'country' => 'Lorem Ipsum Country',
                ]
            ],
            'whenDocumentIsEmpty' => [
                'data' => [
                    'name' => 'John Doe',
                    'rg_ie' => '999',
                    'birth_date' => '2020-09-09',
                    'phone' => '9912345678',
                    'password' => '123',
                    'address' => 'Lorem Ipsum Address',
                    'address_number' => 1,
                    'city' => 'Lorem Ipsum City',
                    'country' => 'Lorem Ipsum Country',
                ]
            ],
            'whenBirthDateIsEmpty' => [
                'data' => [
                    'name' => 'John Doe',
                    'cpf_cnpj' => '999',
                    'rg_ie' => '999',
                    'phone' => '9912345678',
                    'password' => '123',
                    'address' => 'Lorem Ipsum Address',
                    'address_number' => 1,
                    'city' => 'Lorem Ipsum City',
                    'country' => 'Lorem Ipsum Country',
                ]
            ],
            'whenPhoneIsEmpty' => [
                'data' => [
                    'name' => 'John Doe',
                    'cpf_cnpj' => '999',
                    'rg_ie' => '999',
                    'birth_date' => '2020-09-09',
                    'password' => '123',
                    'address' => 'Lorem Ipsum Address',
                    'address_number' => 1,
                    'city' => 'Lorem Ipsum City',
                    'country' => 'Lorem Ipsum Country',
                ]
            ],
            'whenPasswordIsEmpty' => [
                'data' => [
                    'name' => 'John Doe',
                    'cpf_cnpj' => '999',
                    'rg_ie' => '999',
                    'birth_date' => '2020-09-09',
                    'phone' => '9912345678',
                    'address' => 'Lorem Ipsum Address',
                    'address_number' => 1,
                    'city' => 'Lorem Ipsum City',
                    'country' => 'Lorem Ipsum Country',
                ]
            ]
        ];
    }

    /**
     * @test
     * @dataProvider documentProvider
     */
    public function shouldBeCheckIfDocumentExists($data, $expected)
    {
        $userDAO = new UserDAO(self::$pdo);

        $documentExists = $userDAO->documentExists($data);

        $this->assertEquals($expected, $documentExists);
    }

    public function documentProvider()
    {
        return [
            'documentExistsIsTrue' => [
                'data' => '999',
                'expected' => true
            ],
            'documentExistsIsFalse' => [
                'data' => '000',
                'expected' => false
            ]
        ];
    }

    public static function tearDownAfterClass(): void
    {
        self::$pdo->exec('DROP DATABASE ' . DB_DBNAME_TEST);
    }
}
