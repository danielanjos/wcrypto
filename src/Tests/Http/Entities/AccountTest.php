<?php

namespace DanielAnjos\WCrypto\Tests\Http\Entities;

use DanielAnjos\WCrypto\Http\Entities\Account;
use DanielAnjos\WCrypto\Http\Entities\User;
use PHPUnit\Framework\TestCase;

class AccountTest extends TestCase
{
    /**
     * @test
     * @dataProvider accountDataProvider
     */
    public function shouldFillClassAttributesWhenKeyEqualsAttrName($data)
    {
        $account = new Account($data);

        $this->assertEquals(1, $account->id);
        $this->assertEquals('123', $account->account_number);
        $this->assertEquals(1.50, $account->balance);
        $this->objectHasAttribute('id', $account->user);
    }

    /**
     * @test
     */
    public function shouldFillIdAttributeWhenSetterIsCalled()
    {
        $user = new Account([]);

        $user->setId(1);

        $user->setBalance(1.50);

        $this->assertEquals(1, $user->id);
        $this->assertEquals(1.50, $user->balance);
    }

    /**
     * @test
     * @dataProvider accountDataProvider
     */
    public function shouldBeAbleToShowUserWhenJsonEncodeIsCalled($data)
    {
        $expectedJson = json_encode($data);

        $user = new Account($data);

        $userJson = json_encode($user);

        $this->assertEquals($expectedJson, $userJson);
    }

    public function accountDataProvider()
    {
        return [
            'accountData' => ['data' => [
                'id' => 1,
                'account_number' => '123',
                'balance' => 1.50,
                'user' => new User([])
            ]]
        ];
    }
}
