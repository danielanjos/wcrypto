<?php

namespace DanielAnjos\WCrypto\Tests\Http\Entities;

use DanielAnjos\WCrypto\Http\Entities\Account;
use DanielAnjos\WCrypto\Http\Entities\Transaction;
use DanielAnjos\WCrypto\Http\Entities\User;
use PHPUnit\Framework\TestCase;

class TransactionTest extends TestCase
{
    /**
     * @test
     * @dataProvider transactionDataProvider
     */
    public function shouldFillClassAttributesWhenKeyEqualsAttrName($data)
    {
        $transaction = new Transaction($data);

        $this->assertEquals(1, $transaction->id);
        $this->assertEquals(1, $transaction->source_account->id);
        $this->assertEquals(100, $transaction->value);
        $this->assertEquals(2, $transaction->target_account->id);
    }

    /**
     * @test
     * @dataProvider transactionDataProvider
     */
    public function shouldFillIdAttributeWhenSetterIsCalled($data)
    {
        $transaction = new Transaction($data);

        $transaction->setId(1);

        $this->assertEquals(1, $transaction->id);
    }

    /**
     * @test
     */
    public function shouldBeAbleToShowUserWhenJsonEncodeIsCalled()
    {
        $data = [
            'id' => 1,
            'account_number' => '123',
            'balance' => 1.50,
            'user' => new User([])
        ];

        $expectedJson = json_encode($data);

        $user = new Account($data);

        $userJson = json_encode($user);

        $this->assertEquals($expectedJson, $userJson);
    }

    /**
     * @test
     * @dataProvider transactionDataProvider
     */
    public function shouldBeAbleToShowTransactionWhenJsonEncodeIsCalled($data)
    {

        $expectedJson = json_encode($data);

        $transaction = new Transaction($data);

        $transactionJson = json_encode($transaction);

        $this->assertEquals($expectedJson, $transactionJson);
    }

    public function transactionDataProvider()
    {
        return [
            'TransactionData' => ['data' => [
                'id' => 1,
                'type' => 'Deposit',
                'source_account' => new Account([
                    'id' => 1,
                    'user' => new User(['id' => 1])
                ]),
                'value' => 100,
                'target_account' => new Account([
                    'id' => 2,
                    'user' => new User(['id' => 2])
                ])
            ]]
        ];
    }
}
