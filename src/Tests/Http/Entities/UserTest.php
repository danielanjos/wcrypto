<?php

namespace DanielAnjos\WCrypto\Tests\Http\Entities;

use DanielAnjos\WCrypto\Http\Entities\User;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    /**
     * @test
     * @dataProvider userDataProvider
     */
    public function shouldFillClassAttributesWhenKeyEqualsAttrNameTest($data)
    {
        $user = new User($data);

        $this->assertEquals(1, $user->id);
        $this->assertEquals('John Doe', $user->name);
        $this->assertEquals('999', $user->cpf_cnpj);
        $this->assertEquals('999', $user->rg_ie);
        $this->assertEquals('2020-09-09', $user->birth_date);
        $this->assertEquals('9912345678', $user->phone);
        $this->assertEquals('123', $user->password);
        $this->assertEquals('Lorem Ipsum Address', $user->address);
        $this->assertEquals(1, $user->address_number);
        $this->assertEquals('Lorem Ipsum City', $user->city);
        $this->assertEquals('Lorem Ipsum Country', $user->country);
    }

    /**
     * @test
     */
    public function shouldFillIdAttributeWhenSetterIsCalled()
    {
        $user = new User([]);

        $user->setId(1);

        $this->assertEquals(1, $user->id);
    }

    /**
     * @test
     * @dataProvider userDataProvider
     */
    public function shouldBeAbleToShowUserWhenJsonEncodeIsCalled($data)
    {
        $expectedJson = json_encode($data);

        $user = new User($data);

        $userJson = json_encode($user);

        $this->assertEquals($expectedJson, $userJson);
    }

    public function userDataProvider(){
        return [
            'UserData' => ['data' => [
                'id' => 1,
                'name' => 'John Doe',
                'cpf_cnpj' => '999',
                'rg_ie' => '999',
                'birth_date' => '2020-09-09',
                'phone' => '9912345678',
                'password' => '123',
                'address' => 'Lorem Ipsum Address',
                'address_number' => 1,
                'city' => 'Lorem Ipsum City',
                'country' => 'Lorem Ipsum Country',
            ]]
        ];
    }
}
