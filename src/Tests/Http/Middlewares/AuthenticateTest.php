<?php

namespace DanielAnjos\WCrypto\Tests\Middlewares;

use DanielAnjos\WCrypto\Exceptions\AuthorizationException;
use DanielAnjos\WCrypto\Http\DAOs\AccountDAO;
use DanielAnjos\WCrypto\Http\Middlewares\Authenticate;
use DanielAnjos\WCrypto\Http\Repositories\AccountsRepository;
use Pecee\Http\Request;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class AuthenticateTest extends TestCase
{

    private $request;
    private $logger;
    private $accountsRepository;

    protected function setUp(): void
    {
        $this->request = $this->createMock(Request::class);
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->accountsRepository = new AccountsRepository(new AccountDAO());
    }

    /**
     * @test
     */
    public function shoudBeAbleToAuthenticateWhenTokenIsValid()
    {
        $token = TEST_VALID_TOKEN;

        $this->request->method('getHeader')
            ->willReturn($token);

        $auth = new Authenticate($this->logger, $this->accountsRepository);

        $this->request->expects($this->once())->method('__set');

        $auth->handle($this->request);
    }

    /**
     * @test
     */
    public function shouldNotBeAbleToAuthenticateWhenTokenIsEmpty()
    {
        $auth = new Authenticate($this->logger, $this->accountsRepository);

        $this->expectException(AuthorizationException::class);

        $auth->handle($this->request);
    }

    /**
     * @test
     */
    public function shouldNotBeAbleToAuthenticateWhenTokenIsInvalid()
    {
        $token = TEST_INVALID_TOKEN;

        $this->request->method('getHeader')
            ->willReturn($token);

        $auth = new Authenticate($this->logger, $this->accountsRepository);

        $this->expectException(AuthorizationException::class);

        $auth->handle($this->request);
    }
}
