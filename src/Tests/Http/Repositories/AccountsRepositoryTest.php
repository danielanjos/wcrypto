<?php

namespace DanielAnjos\WCrypto\Tests\Http\Repositories;

use DanielAnjos\WCrypto\Http\DAOs\AccountDAO;
use DanielAnjos\WCrypto\Http\Entities\Account;
use DanielAnjos\WCrypto\Http\Entities\User;
use DanielAnjos\WCrypto\Http\Repositories\AccountsRepository;
use PHPUnit\Framework\TestCase;

class AccountsRepositoryTest extends TestCase
{

    private $data;
    private $accountDAO;

    protected function setUp(): void
    {
        $this->data = [
            'id' => 1,
            'account_number' => '123',
            'balance' => 1.50,
            'user' => new User([])
        ];

        $this->accountDAO = $this->createMock(AccountDAO::class);
    }

    /**
     * @test
     */
    public function shouldBeAbleToCreateAccountWhenIsCalled()
    {
        $this->accountDAO->method('insert')
            ->willReturn(new Account($this->data));

        $accountsRepository = new AccountsRepository($this->accountDAO);

        $account = $accountsRepository->create($this->data);

        $this->assertObjectHasAttribute('id', $account);
    }
}
