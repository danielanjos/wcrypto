<?php

namespace DanielAnjos\WCrypto\Tests\Http\Repositories;

use DanielAnjos\WCrypto\Http\DAOs\SessionsDAO;
use DanielAnjos\WCrypto\Http\Repositories\SessionsRepository;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class SessionsRepositoryTest extends  TestCase
{
    /**
     * @test
     */
    public function shouldBeAbleToSignInWhenDocumentAndPasswordIsCorrectTest()
    {

        $data = [
            'document' => '483.583.508-56',
            'password' => '123'
        ];

        $expectedResult = TEST_VALID_TOKEN;

        $repository = new SessionsRepository(new SessionsDAO());

        $response = $repository->signIn($data);

        $this->assertEquals($expectedResult, $response["token"]);
    }

    /**
     * @test
     * @dataProvider signInDataProvider
     */
    public function shouldNotBeAbleToSignInTest($data, $expectedResult)
    {
        $repository = new SessionsRepository(new SessionsDAO());

        $this->expectException($expectedResult);
        $repository->signIn($data);
    }

    public function signInDataProvider()
    {
        return [
            'shouldNotBeAbleToLoginWhenPasswordIsIncorrect' => [
                'data' => [
                    'document' => '483.583.508-56',
                    'password' => '1234'
                ],
                'expectedResult' => InvalidArgumentException::class
            ],
            'shouldNotBeAbleToLoginWhenUserNotExists' => [
                'data' => [
                    'document' => '000',
                    'password' => '1234'
                ],
                'expectedResult' => InvalidArgumentException::class
            ],
            'shouldNotBeAbleToLoginWhenIsEmptyData' => [
                'data' => [
                    'document' => '',
                    'password' => ''
                ],
                'expectedResult' => InvalidArgumentException::class
            ]
        ];
    }
}
