<?php

namespace DanielAnjos\WCrypto\Tests\Http\Repositories;

use DanielAnjos\WCrypto\Http\DAOs\AccountDAO;
use DanielAnjos\WCrypto\Http\DAOs\TransactionDAO;
use DanielAnjos\WCrypto\Http\Entities\Account;
use DanielAnjos\WCrypto\Http\Entities\Transaction;
use DanielAnjos\WCrypto\Http\Entities\User;
use DanielAnjos\WCrypto\Http\Repositories\TransactionsRepository;
use Exception;
use PHPUnit\Framework\TestCase;

class TransactionsRepositoryTest extends TestCase
{
    private $transactionDAO;
    private $accountDAO;
    private $source_account;
    private $target_account;

    protected function setUp(): void
    {
        $this->transactionDAO = $this->createMock(TransactionDAO::class);

        $this->accountDAO = $this->createMock(AccountDAO::class);

        $this->source_account = new Account([
            'id' => 1,
            'account_number' => 1,
            'balance' => 1000,
            'user' => new User(['id' => 1])
        ]);

        $this->target_account = new Account([
            'id' => 2,
            'account_number' => 2,
            'balance' => 5000,
            'user' => new User(['id' => 2])
        ]);
    }

    /**
     * @test
     */
    public function shouldBeAbleToMakeDeposit()
    {
        $data = [
            'source_account_number' => 1,
            'type' => 'Deposit',
            'value' => 1000
        ];

        $deposit_return = new Transaction([
            'id' => 1,
            'type' => 'Deposit',
            'source_account' => $this->source_account->setBalance(2000),
            'target_account' => $this->source_account
        ]);

        $this->accountDAO->expects($this->at(0))
            ->method('getAccountByNumber')
            ->willReturn($this->source_account);

        $this->accountDAO->method('insert')
            ->willReturn($deposit_return);

        $transactionsRepository = new TransactionsRepository(
            $this->transactionDAO,
            $this->accountDAO
        );

        $transaction = $transactionsRepository->create($data);

        $this->assertEquals(2000, $transaction->source_account->balance);
    }

    /**
     * @test
     */
    public function shouldBeAbleToMakeWithdrawal()
    {
        $data = [
            'source_account_number' => 1,
            'type' => 'Withdrawal',
            'value' => 500
        ];

        $withdrawal_return = new Transaction([
            'id' => 1,
            'type' => 'Deposit',
            'source_account' => $this->source_account->setBalance(500),
            'target_account' => $this->source_account
        ]);

        $this->accountDAO->expects($this->at(0))
            ->method('getAccountByNumber')
            ->willReturn($this->source_account);

        $this->accountDAO->method('insert')
            ->willReturn($withdrawal_return);

        $transactionsRepository = new TransactionsRepository(
            $this->transactionDAO,
            $this->accountDAO
        );

        $transaction = $transactionsRepository->create($data);

        $this->assertEquals(500, $transaction->source_account->balance);
    }

    /**
     * @test
     */
    public function shouldNotBeAbleToMakeWithdrawalWhenBalanceIsInsufficient()
    {
        $data = [
            'source_account_number' => 1,
            'type' => 'Withdrawal',
            'value' => 5000
        ];

        $this->accountDAO->expects($this->at(0))
            ->method('getAccountByNumber')
            ->willReturn($this->source_account);

        $transactionsRepository = new TransactionsRepository(
            $this->transactionDAO,
            $this->accountDAO
        );

        $this->expectExceptionMessage('Insufficient balance');

        $transactionsRepository->create($data);
    }

    /**
     * @test
     */
    public function shouldBeAbleToMakeTransfer()
    {
        $data = [
            'source_account_number' => 1,
            'type' => 'Transfer',
            'value' => 500,
            'target_account_number' => 2
        ];

        $transfer_return = new Transaction([
            'id' => 1,
            'type' => 'Transfer',
            'source_account' => $this->source_account->setBalance(500),
            'target_account' => $this->target_account->setBalance(4500)
        ]);

        $this->accountDAO->expects($this->at(0))
            ->method('getAccountByNumber')
            ->willReturn($this->source_account);

        $this->accountDAO->expects($this->at(1))
            ->method('getAccountByNumber')
            ->willReturn($this->target_account);

        $this->accountDAO->method('insert')
            ->willReturn($transfer_return);

        $transactionsRepository = new TransactionsRepository(
            $this->transactionDAO,
            $this->accountDAO
        );

        $transaction = $transactionsRepository->create($data);

        $this->assertEquals(500, $transaction->source_account->balance);
        $this->assertEquals(4500, $transaction->target_account->balance);
    }

    /**
     * @test
     */
    public function shouldNotBeAbleToMakeTransferWhenBalanceIsInsufficient()
    {
        $data = [
            'source_account_number' => 1,
            'type' => 'Transfer',
            'value' => 5000,
            'target_account_number' => 2
        ];

        $this->accountDAO->expects($this->at(0))
            ->method('getAccountByNumber')
            ->willReturn($this->source_account);

        $this->accountDAO->expects($this->at(1))
            ->method('getAccountByNumber')
            ->willReturn($this->target_account);


        $transactionsRepository = new TransactionsRepository(
            $this->transactionDAO,
            $this->accountDAO
        );

        $this->expectExceptionMessage('Insufficient balance');

        $transactionsRepository->create($data);
    }

    /**
     * @test
     */
    public function shoultNotBeAbleToMakeTransactionWhenTypeNotExists()
    {
        $data = [
            'source_account_number' => 1,
            'type' => '',
            'value' => 500
        ];

        $this->accountDAO->expects($this->at(0))
            ->method('getAccountByNumber')
            ->willReturn($this->source_account);

        $transactionsRepository = new TransactionsRepository(
            $this->transactionDAO,
            $this->accountDAO
        );

        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Invalid transaction');

        $transactionsRepository->create($data);
    }
}
