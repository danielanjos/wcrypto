<?php

namespace DanielAnjos\WCrypto\Tests\Http\Repositories;

use DanielAnjos\WCrypto\Http\DAOs\UserDAO;
use DanielAnjos\WCrypto\Http\Entities\User;
use DanielAnjos\WCrypto\Http\Repositories\UsersRepository;
use Exception;
use PHPUnit\Framework\TestCase;

class UsersRepositoryTest extends TestCase
{

    private $data;
    private $userDAO;

    protected function setUp(): void
    {
        $this->data = [
            'id' => 1,
            'name' => 'John Doe',
            'cpf_cnpj' => '999',
            'rg_ie' => '999',
            'birth_date' => '2020-09-09',
            'phone' => '9912345678',
            'password' => '123',
            'address' => 'Lorem Ipsum Address',
            'address_number' => 1,
            'city' => 'Lorem Ipsum City',
            'country' => 'Lorem Ipsum Country',
        ];

        $this->userDAO = $this->createMock(UserDAO::class);
    }

    /**
     * @test
     */
    public function shouldBeAbleToCreateAUserWhenDocumentNotExists()
    {
        $this->userDAO->method('insert')
            ->willReturn(new User($this->data));

        $usersRepository = new UsersRepository($this->userDAO);

        $user = $usersRepository->create($this->data);

        $this->assertObjectHasAttribute('id', $user);
    }

    /**
     * @test
     */
    public function shouldNotBeAbleToCreateAUserWhenDocumentExists()
    {
        $this->userDAO->method('documentExists')
            ->willReturn(true);

        $usersRepository = new UsersRepository($this->userDAO);

        $this->expectException(Exception::class);

        $usersRepository->create($this->data);
    }
}
