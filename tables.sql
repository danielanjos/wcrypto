CREATE TABLE users (
	id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    cpf_cnpj VARCHAR(255) UNIQUE NOT NULL,
    rg_ie VARCHAR(255),
    birth_date DATE NOT NULL,
    phone VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    address VARCHAR(255),
    address_number VARCHAR(255),
    city VARCHAR(255),
    country VARCHAR(255)
);

CREATE TABLE accounts(
	id INT PRIMARY KEY AUTO_INCREMENT,
    account_number VARCHAR(255),
    balance DECIMAL(10,2) DEFAULT 0,
    user_id INT UNIQUE,
    CONSTRAINT FK_AccountUser FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE transactions(
	id INT PRIMARY KEY AUTO_INCREMENT,
    source_account_id INT,
    type varchar(30),
    value double,
    target_account_id INT,
    CONSTRAINT FK_TransactionSourceAccount FOREIGN KEY (source_account_id) REFERENCES accounts(id),
    CONSTRAINT FK_TransactionTargetAccount FOREIGN KEY (target_account_id) REFERENCES accounts(id)
);
